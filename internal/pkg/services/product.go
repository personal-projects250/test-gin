package services

import (
	"net/http"

	"gitlab.com/personal-projects250/test-gin/internal/pkg/models/request"
	"gitlab.com/personal-projects250/test-gin/internal/pkg/models/response"
	"gitlab.com/personal-projects250/test-gin/internal/pkg/repositories/product"
)

type ProductsService struct{}

func (s *ProductsService) GetProducts() (*response.ProductsResponse, *response.ErrorResponse) {

	r := product.NewProductsRepository()
	rsp, err := r.Get()
	if err != nil {
		return nil, response.NewErrorResponse(http.StatusBadRequest, err.Error())
	}
	return rsp, nil
}

func (s *ProductsService) NewProduct(rqt *request.ProductRequest) (*response.ProductResponse, *response.ErrorResponse) {
	if len(rqt.Name) < 1 {
		return nil, response.NewErrorResponse(http.StatusBadRequest, "Product name cannot be empty!")
	}

	r := product.NewProductsRepository()
	rsp, err := r.Insert(rqt)
	if err != nil {
		return nil, response.NewErrorResponse(http.StatusBadRequest, err.Error())
	}
	return rsp, nil
}

func (s *ProductsService) UpdateProduct(rqt *request.ProductRequest) (*response.ProductResponse, *response.ErrorResponse) {

	r := product.NewProductsRepository()
	rsp, err := r.Update(rqt)
	if err != nil {
		return nil, response.NewErrorResponse(http.StatusBadRequest, err.Error())
	}
	return rsp, nil
}

func (s *ProductsService) DeleteProduct(rqt *request.ProductRequest) (*response.ProductResponse, *response.ErrorResponse) {

	r := product.NewProductsRepository()
	rsp, err := r.Delete(rqt)
	if err != nil {
		return nil, response.NewErrorResponse(http.StatusBadRequest, err.Error())
	}
	return rsp, nil
}
