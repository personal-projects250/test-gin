package product

import (
	"gitlab.com/personal-projects250/test-gin/internal/pkg/models/request"
	"gitlab.com/personal-projects250/test-gin/internal/pkg/models/response"
	"gitlab.com/personal-projects250/test-gin/internal/pkg/repositories/config"
)

type ProductsRepository struct {
	instance *config.ConfigRepository
}

func NewProductsRepository() *ProductsRepository {
	return &ProductsRepository{
		instance: config.GetConfigRepository(),
	}
}

func (r *ProductsRepository) Get() (*response.ProductsResponse, error) {

	query := "SELECT * FROM products"

	results, err := r.instance.DB.Query(query)
	if err != nil {
		return nil, err
	}

	rsp := new(response.ProductsResponse)
	for results.Next() {
		product := new(response.ProductResponse)
		if err := results.Scan(&product.ID, &product.Name, &product.Price); err != nil {
			continue
		}
		rsp.Products = append(rsp.Products, product)
	}
	results.Close()
	return rsp, nil
}

func (r *ProductsRepository) Insert(rqt *request.ProductRequest) (*response.ProductResponse, error) {

	query := `INSERT INTO products(name, price) VALUES (?, ?);`

	res, err := r.instance.DB.Exec(query, rqt.Name, rqt.Price)
	if err != nil {
		return nil, err
	}

	lastID, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}

	return &response.ProductResponse{
		ID:    int(lastID),
		Name:  rqt.Name,
		Price: rqt.Price,
	}, nil
}

func (r *ProductsRepository) Update(rqt *request.ProductRequest) (*response.ProductResponse, error) {

	query := "UPDATE products SET name = ?, price = ? WHERE id = ?"

	if _, err := r.instance.DB.Exec(query, rqt.Name, rqt.Price, rqt.ID); err != nil {
		return nil, err
	}

	return &response.ProductResponse{
		ID:    rqt.ID,
		Name:  rqt.Name,
		Price: rqt.Price,
	}, nil
}

func (r *ProductsRepository) Delete(rqt *request.ProductRequest) (*response.ProductResponse, error) {

	query := "DELETE FROM products WHERE id  = ? "

	if _, err := r.instance.DB.Exec(query, rqt.ID); err != nil {
		return nil, err
	}

	return &response.ProductResponse{
		ID:    rqt.ID,
		Name:  rqt.Name,
		Price: rqt.Price,
	}, nil
}
