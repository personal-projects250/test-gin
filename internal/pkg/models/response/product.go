package response

type ProductResponse struct {
	ID    int     `json:"id"`
	Name  string  `json:"name"`
	Price float64 `json:"price"`
}

type ProductsResponse struct {
	Products []*ProductResponse `json:"products"`
}
