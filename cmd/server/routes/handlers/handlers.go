package handlers

import (
	"net/http"

	"gitlab.com/personal-projects250/test-gin/internal/pkg/models/request"
	"gitlab.com/personal-projects250/test-gin/internal/pkg/models/response"
	"gitlab.com/personal-projects250/test-gin/internal/pkg/services"

	"github.com/gin-gonic/gin"
)

type Handler struct {
	ProductService *services.ProductsService
}

func NewHandlers() *Handler {
	return &Handler{}
}

func (h *Handler) NewProduct(c *gin.Context) {

	rqt := new(request.ProductRequest)
	if err := c.BindJSON(rqt); err != nil {
		c.JSON(http.StatusBadRequest, response.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
		return
	}

	r, e := h.ProductService.NewProduct(rqt)
	if e != nil {
		c.JSON(e.Status, e)
		return
	}
	c.JSON(http.StatusOK, r)
}

func (h *Handler) GetProducts(c *gin.Context) {
	r, e := h.ProductService.GetProducts()
	if e != nil {
		c.JSON(e.Status, e)
		return
	}
	c.JSON(http.StatusOK, r)
}

func (h *Handler) UpdateProduct(c *gin.Context) {

	rqt := new(request.ProductRequest)
	if err := c.BindJSON(rqt); err != nil {
		c.JSON(http.StatusBadRequest, response.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
		return
	}

	r, e := h.ProductService.UpdateProduct(rqt)
	if e != nil {
		c.JSON(e.Status, e)
		return
	}
	c.JSON(http.StatusOK, r)

}
func (h *Handler) DeleteProduct(c *gin.Context) {

	rqt := new(request.ProductRequest)
	if err := c.BindJSON(rqt); err != nil {
		c.JSON(http.StatusBadRequest, response.ErrorResponse{
			Status:  http.StatusBadRequest,
			Message: err.Error(),
		})
		return
	}

	r, e := h.ProductService.DeleteProduct(rqt)
	if e != nil {
		c.JSON(e.Status, e)
		return
	}
	c.JSON(http.StatusOK, r)
}
