package routes

import (
	"gitlab.com/personal-projects250/test-gin/cmd/server/routes/handlers"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type Routes struct {
	Engine  *gin.Engine
	Handler *handlers.Handler
}

func NewRoutes() *Routes {

	engine := gin.Default()
	engine.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{"Origin", "Content-Type", "Accept"},
	}))

	return &Routes{
		Engine:  engine,
		Handler: handlers.NewHandlers(),
	}
}

func (r *Routes) RegisterRoutes(group string) {
	api := r.Engine.Group(group)
	api.GET("products", r.Handler.GetProducts)
	api.POST("products", r.Handler.NewProduct)
	api.PATCH("products", r.Handler.UpdateProduct)
	api.DELETE("products", r.Handler.DeleteProduct)
}
